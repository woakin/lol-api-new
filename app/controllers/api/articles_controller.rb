class API::ArticlesController < ApplicationController
  def index
    @articles = Article.order(:position).last(3)
    render json: @articles, :callback  => params[:callback]
  end
end
