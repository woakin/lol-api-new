class API::CalendarsController < ApplicationController
  def show

    resp = []

    Journey.all.order(:id).each do |single_journey|

      a =[]
      b = []
      c = []
      d = []

      single_journey.matches.each do |game|
        to_array = {
            score_a: game.score_a,
            score_b: game.score_b,
            time: game.time,
            teams: game.teams
        }

        if game.a_team.group.name == "A"
          a.push to_array
        elsif game.a_team.group.name == "B"
          b.push to_array
        elsif game.a_team.group.name == "C"
          c.push to_array
        elsif game.a_team.group.name == "D"
          d.push to_array
        end
      end


      if  single_journey.name == "Eliminatorias" or single_journey.name == "Final"

        json1 = {
            id:single_journey.id,
            name:single_journey.name,
            date: single_journey.date,
            need_brackets:single_journey.need_brackets,
            brackets_url:single_journey.brackets_url,

            groups:[
                {
                    group: 'A',
                    matches: a +b+ c + d
                }
            ]
        }

      else
        json1 = {
            id:single_journey.id,
            name:single_journey.name,
            date: single_journey.date,
            need_brackets:single_journey.need_brackets,
            brackets_url:single_journey.brackets_url,


            groups:[
                {
                    group: 'A',
                    matches: a
                },
                {
                    group: 'B',
                    matches: b
                },
                {
                    group: 'C',
                    matches: c
                },
                {
                    group: 'D',
                    matches: d
                }

            ]
        }

      end

      resp << json1
    end



    render json: resp
  end

  def old
    @calendar = Journey.all.order(:id)
    render json: @calendar, include: { matches: {
        include: { teams: { only: [:name,:logo_url]} },
        only: [:score_a,:score_b,:time] } }
  end
end
