class API::MenusController < ApplicationController

  def show
    objects = Menu.all.select(:id,:title,:active,:parent_menu_id).order(:id).where('active', '=', true)

    objects = objects.as_json

    objects.collect! do |child|
      if not child['parent_menu_id'].nil? then
        parents = objects.select{|o| o['id'] == child['parent_menu_id']}
        parents.each do |pr|
          pr['submenus'] ||= []
          pr['submenus'] << child
        end
      end
      child
    end.select!{|o| o['parent_menu_id'].nil? }
    render json: objects
  end
end
