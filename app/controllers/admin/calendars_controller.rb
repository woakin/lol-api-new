class Admin::CalendarsController < ApplicationController
  before_filter :authorize
  
  def edit
    @teams = Team.select(:id,:name,:victories,:defeats,:kills,:deaths,:points,:change_place_img).order(:name)
  end
  def update

        errors = []
        calendar_params[:teams].each do |team_params|
          team = Team.find(team_params[:id])
          unless team.update team_params
            errors.push [team.name,team.errors]
          end
        end

        if errors.empty?
          flash[:success] = "Editados"
        else
          flash[:danger] = errors
        end

        redirect_to edit_admin_calendar_path
  end

  def calendar_params
    params.permit(teams: [:id,:name,:victories,:defeats,:kills,:deaths,:points,:change_place_img])
  end
end
