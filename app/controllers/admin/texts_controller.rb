class Admin::TextsController < ApplicationController
  before_filter :authorize
  def edit
    @texts = Text.first
  end

  def update

    text  = Text.first

    if text.update text_params
      flash[:success] = "Editado"
    else
      flash[:danger] = text.errors
    end

    redirect_to edit_admin_texts_path
  end

  def text_params
    params.permit(:copa_header,:global_rules,:local_rules,:cim,:cdl)
  end
end
