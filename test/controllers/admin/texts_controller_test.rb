require 'test_helper'

class Admin::TextsControllerTest < ActionDispatch::IntegrationTest
  test "should get edit" do
    get admin_texts_edit_url
    assert_response :success
  end

  test "should get update" do
    get admin_texts_update_url
    assert_response :success
  end

end
