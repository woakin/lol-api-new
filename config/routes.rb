Rails.application.routes.draw do
  namespace :admin do
    get 'texts/edit'
  end

  namespace :admin do
    get 'texts/update'
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api, :defaults => {:format => :json}  do
    resource :calendar, only: [:show] do
      collection do
        get 'old'
      end
    end
    resources :teams, only:[:index] do
      collection do
        get 'by_points'
        get 'by_group'
      end
    end
    resources :articles, only: [:index]

    resource :leaderboard, only: [:show]

    resource :options, only:[:show]

    resource :texts, only: [:show]

    resource :menu, only:[:show]

    resources :matches, only: [] do
      collection do
        get 'upcoming'
      end
    end
  end

  namespace :admin do
    resources :teams
    resources :players
    resources :matches
    resources :journeys
    resources :articles do
      member do
        post :move
      end
    end
    resource :calendar, only:[:edit,:update]
    resource :options, only:[:edit,:update]
    resource :texts, only:[:edit,:update]
    resources :groups
    resource :menu, only:[:edit,:update]
  end

  resource :session, only:[:create,:new,:destroy]

  root to: 'sessions#new'
end
