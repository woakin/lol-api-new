class CreateMenus < ActiveRecord::Migration[5.0]
  def change
    create_table :menus do |t|
      t.string :title
      t.boolean :active
      t.integer :parent_menu_id

      t.timestamps
    end
  end
end
