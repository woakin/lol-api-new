class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.string :url
      t.boolean :is_youtube, default: false
      t.string :thumb_url
      t.string :title
      t.string :subtitle
      t.date :date

    end
  end
end
