class AddGlobalAndLocalRulesToTexts < ActiveRecord::Migration[5.0]
  def change
    add_column :texts, :global_rules, :text
    add_column :texts, :local_rules, :text
  end
end
